# docker_transcendance

## Getting started

Docker_Transcendance is a Docker environment set up for a final project at 42. The project includes a Docker Compose file that defines three services: api-node, web-node, and postgres, as well as a network called my_network.

The api-node service is built from a Dockerfile located in the back-end directory, and has its source code directory mounted as a volume. It depends on the postgres service and exposes ports 3000 and 5555. The web-node service is built from a Dockerfile located in the front-end directory, and has its source code directory mounted as a volume. It depends on the api-node service and exposes port 4000.

The postgres service uses the postgres:13-bullseye image and sets several environment variables. It has a healthcheck configured to check if the service is healthy and uses a named volume to persist the database data.

The Dockerfile for the api-node service is based on the node:14-alpine image and installs several global and local dependencies. The Dockerfile for the web-node service is based on the node:16-alpine image and installs both development and production dependencies.

Overall, this Docker environment sets up a development environment for a full-stack web application, with a Node.js backend, a React frontend, and a PostgreSQL database.

I deliberately included the .env file and modified the value of the variables as they are sensitive.

## docker-compose description

This docker-compose file defines three services: api-node, web-node, and postgres, and also defines a network called my_network.

The api-node service is built from a Dockerfile located in ../../back-end directory, and has its source code directory (../../back-end/src) mounted as a volume. It depends on the postgres service and exposes port 3000 and 5555. It also sets several environment variables: DATABASE_URL, JWT_SECRET, FORTYTWO_APP_ID, FORTYTWO_APP_SECRET, FORTYTWO_CALLBACK_URL, and FRONTEND_URL.

The web-node service is built from a Dockerfile located in ../../front-end directory, and has its source code directory (../../front-end/src) mounted as a volume. It depends on the api-node service and exposes port 4000. It sets the BACKEND_URL environment variable.

The postgres service uses the postgres:13-bullseye image and exposes port 5432. It also sets several environment variables: POSTGRES_USER, POSTGRES_PASSWORD, and POSTGRES_DB. The service uses a named volume called db-data to persist the database data. It has a healthcheck configured to check if the service is healthy.

All three services are connected to a custom bridge network called my_network. The api-node service has an alias of back_hostname, the web-node service has an alias of front_hostname, and the postgres service has an alias of db_hostname.

Overall, this docker-compose file sets up a development environment for a full-stack web application, with a Node.js backend (api-node), a React frontend (web-node), and a PostgreSQL database (postgres).

## backEnd Dockerfile
This is a Dockerfile for a Node.js application. The FROM statement specifies that the base image is node:14-alpine, which is a lightweight version of Node.js version 14.

The ARG statements define several build-time variables that can be passed in when building the Docker image. The ENV statements set several environment variables for the container, based on the values of the build-time variables.

The WORKDIR statement sets the working directory for subsequent instructions to /app, and the PATH statement adds /app/node_modules/.bin to the PATH environment variable.

The COPY statements copy the package.json, prisma, and application code files to the /app directory.

The RUN statements run several commands to install global dependencies (prisma and @nestjs/cli) and local dependencies (npm install) and generate a Prisma client.

The EXPOSE statement indicates that the container listens on port 3000, and the CMD statement specifies the command to run when the container starts (npm run start:dev-prisma).

## frontEnd Dockerfile
This is a Dockerfile for a Node.js application. The FROM statement specifies that the base image is node:16-alpine, which is a lightweight version of Node.js version 16.

The ARG statement defines a build-time variable BACKEND_URL, which can be passed in when building the Docker image. The ENV statements set several environment variables for the container, based on the value of the BACKEND_URL build-time variable.

The WORKDIR statement sets the working directory for subsequent instructions to /app, and the PATH statement adds /app/node_modules/.bin to the PATH environment variable.

The COPY statements copy the package.json and application code files to the /app directory.

The RUN statement runs npm install with the --production=false flag to install both development and production dependencies.

The EXPOSE statement indicates that the container listens on port 4000, and the CMD statement specifies the command to run when the container starts (npm run dev -- --port 4000). This runs the application in development mode with a specific port, which is passed in as a command-line argument.